﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Textadventure_2023
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string Frage_1;
            string Frage_2;

            Console.WriteLine("Du musst zur Arbeit und bist schon fast zu spät dran. Wirst du die gefährliche Abkürzung nehmen, um schneller zu sein? (ja, nein)");

            Frage_1 = Console.ReadLine();

            if (Frage_1 == "ja")
            {
                Console.WriteLine("Du nimmst die Abkürzung.");
                Console.WriteLine("Es kommen ein paar Leute in schwarzen Kaputzenpullovern und schlagen dich zusammen. ENDE");
            }
            else if (Frage_1 == "nein")
            {
                Console.WriteLine("Du gehst einfach geradeaus weiter.");
                Console.WriteLine("Du findest eine Baustelle, die genau auf deinem Weg ist. Gibst du einfach auf oder gehst du durch die Baustelle(aufgeben, durchgehen)? ");

                Frage_2 = Console.ReadLine();

                if (Frage_2 == "aufgeben") Console.WriteLine("Du gibst auf und kommst heute nicht mehr in die Arbeit. ENDE");
                if (Frage_2 == "durchgehen") Console.WriteLine("Du gehst durch die Baustelle und schaffst es erstaunlicherweise heute noch in die Arbeit. ENDE");
            }



            //Test22222
            //asdf

            Console.ReadKey();
        }
    }
}
